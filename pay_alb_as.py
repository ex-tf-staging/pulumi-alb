import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
def pay_alb(resource_name, {}):
 const frontEndLoadBalancer = new aws.lb.LoadBalancer("frontEnd", {});
 const frontEndTargetGroup = new aws.lb.TargetGroup("frontEnd", {});
 const frontEndListener = new aws.lb.Listener("frontEnd", {
    certificateArn: "arn:aws:iam::187416307283:server-certificate/test_cert_rab3wuqwgja25ct3n4jdj2tzu4",
    defaultActions: [{
        targetGroupArn: frontEndTargetGroup.arn,
        type: "forward",
    }],
    loadBalancerArn: frontEndLoadBalancer.arn,
    port: 443,
    protocol: "HTTPS",
    sslPolicy: "ELBSecurityPolicy-2016-08",
 });

def pay_as(resource_name, desired_capacity=None, force_delete=None, health_check_grace_period=None, health_check_type=None, initial_lifecycle_hooks=None, launch_configuration=None, max_instance_lifetime=None, max_size=None, min_size=None, mixed_instances_policy=None, name=None, name_prefix=None, placement_group=None, target_group_arns=None):
 const test = new aws.ec2.PlacementGroup("test", {
    strategy: "cluster",
 });
 const bar = new aws.autoscaling.Group("bar", {
    desiredCapacity: desired_capacity,
    forceDelete: force_delete,
    healthCheckGracePeriod: health_check_grace_period,
    healthCheckType: health_check_type,
    initialLifecycleHooks: [{
        defaultResult: "CONTINUE",
        heartbeatTimeout: 2000,
        lifecycleTransition: "autoscaling:EC2_INSTANCE_LAUNCHING",
        name: "foobar",
        notificationMetadata: `{
  "foo": "bar"
 }
`,
        notificationTargetArn: "arn:aws:sqs:us-east-1:444455556666:queue1*",
        roleArn: "arn:aws:iam::123456789012:role/S3Access",
    }],
    launchConfiguration: aws_launch_configuration_foobar.name,
    maxSize: max_size,
    minSize: min_size,
    placementGroup: test.id,
    tags: [
        {
            key: "foo",
            propagateAtLaunch: true,
            value: "bar",
        },
        {
            key: "lorem",
            propagateAtLaunch: false,
            value: "ipsum",
        },
    ],
    vpcZoneIdentifiers: [
        aws_subnet_example1.id,
        aws_subnet_example2.id,
    ],
}, {timeouts: {
    delete: "15m",
}});